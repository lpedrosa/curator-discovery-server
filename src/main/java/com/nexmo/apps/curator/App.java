package com.nexmo.apps.curator;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.curator.RetryPolicy;
import org.apache.curator.retry.RetryNTimes;
import org.apache.curator.utils.CloseableUtils;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import com.nexmo.apps.curator.context.ServiceDiscoveryConfig;
import com.nexmo.apps.curator.context.ServiceDiscoveryContext;
import com.nexmo.apps.curator.resources.NoPayloadDiscoveryResource;

public class App {

    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    private static final String DEFAULT_PROPERTIES_FILENAME = "start.properties";

    // for readability (should really be an enum)
    private static final boolean ACCEPT_EMPTY = true;
    private static final boolean FAIL_ON_EMPTY = false;

    public static void main(String[] args) {

        Properties prop = null;

        try {
            prop = readPropertiesFile(args);
        } catch (Exception e) {
            LOGGER.error("Failed to read properties file... ", e);
            System.exit(1);
        }

        String connectionString = null;
        int sessionTimeout = 0;
        int connectionTimeout = 0;

        String serviceBasePath = null;
        int instanceRefreshTime = 0;

        int port = 0;

        try {
            connectionString = readString(prop, DefaultProperties.ZOOKEEPER_CONNECTION_STRING, FAIL_ON_EMPTY);
            sessionTimeout = readInt(prop, DefaultProperties.ZOOKEEPER_SESSION_TIMEOUT);
            connectionTimeout = readInt(prop, DefaultProperties.ZOOKEEPER_CONNECTION_TIMEOUT);
            serviceBasePath = readString(prop, DefaultProperties.SERVICE_BASE_PATH, ACCEPT_EMPTY);
            instanceRefreshTime = readInt(prop, DefaultProperties.JANITOR_REFRESH);
            port = readInt(prop, DefaultProperties.JETTY_MAIN_PORT);
        } catch (Exception e) {
            LOGGER.error("Failed to initialize properties...", e);
            System.exit(1);
        }

        RetryPolicy retryPolicy = new RetryNTimes(3, 1000);

        ServiceDiscoveryConfig config = new ServiceDiscoveryConfig.Builder().withConnectionString(connectionString)
                                                                            .withSessionTimeout(sessionTimeout)
                                                                            .withConnectionTimeout(connectionTimeout)
                                                                            .withRetryPolicy(retryPolicy)
                                                                            .withServiceBasePath(serviceBasePath)
                                                                            .withInstanceRefreshTime(instanceRefreshTime)
                                                                            .build();

        ServiceDiscoveryContext serviceDiscoveryContext = new ServiceDiscoveryContext(config);

        ResourceConfig resourceConfig = registerResources(serviceDiscoveryContext);

        Server server = configureJetty(resourceConfig, port);

        try {
            LOGGER.info("Starting service discovery context...");
            serviceDiscoveryContext.start();
            LOGGER.info("Starting Jetty server on port {}...", port);
            server.start();
            server.join();
        } catch (Exception ex) {
            LOGGER.error("Startup failed... ", ex);
        } finally {
            serviceDiscoveryContext.stop();
        }

    }

    private static Properties readPropertiesFile(String[] args) throws Exception {
        Properties prop = new Properties(getDefaultProperties());

        InputStream propFileInputStream = null;
        if (args.length != 1) {
            // Read props from classpath
            String filename = DEFAULT_PROPERTIES_FILENAME;
            LOGGER.warn("No properties file provided, reading from classpath [filename: {}]", filename);
            propFileInputStream = App.class.getClassLoader().getResourceAsStream(filename);

            if (propFileInputStream == null)
                throw new Exception("Could not find " + filename + " in the classpath...");
        } else {
            // Read props from a location
            String fileLocation = args[0];

            LOGGER.info("Reading properties from {}", fileLocation);

            try {
                propFileInputStream = new FileInputStream(fileLocation);
            } catch (FileNotFoundException e) {
                CloseableUtils.closeQuietly(propFileInputStream);
                throw e;
            }
        }

        try {
            prop.load(propFileInputStream);
        } catch (IOException e) {
            CloseableUtils.closeQuietly(propFileInputStream);
            throw e;
        }

        CloseableUtils.closeQuietly(propFileInputStream);

        return prop;
    }

    private static Server configureJetty(ResourceConfig resourceConfig, int port) {
        // this is just to set the parent context
        ServletContextHandler contextHandler = new ServletContextHandler(ServletContextHandler.SESSIONS);
        contextHandler.setContextPath("/");

        // register the container in Jetty
        ServletContainer servletContainer = new ServletContainer(resourceConfig);
        ServletHolder servletHolder = new ServletHolder(servletContainer);
        contextHandler.addServlet(servletHolder, "/*");


        Server server = new Server(port);
        server.setHandler(contextHandler);
        return server;
    }

    private static ResourceConfig registerResources(ServiceDiscoveryContext serviceDiscoveryContext) {
        ResourceConfig resourceConfig = new ResourceConfig();

        JacksonJsonProvider jsonProvider = new JacksonJsonProvider();
        resourceConfig.register(jsonProvider);

        NoPayloadDiscoveryResource discoveryResource = new NoPayloadDiscoveryResource(serviceDiscoveryContext.getContextResolver());
        resourceConfig.register(discoveryResource);
        return resourceConfig;
    }

    private static Properties getDefaultProperties() {
        Properties defaultProp = new Properties();

        for (DefaultProperties property : DefaultProperties.values()) {
            defaultProp.setProperty(property.getDefaultKey(), property.getDefaulValue());
        }

        if (LOGGER.isDebugEnabled())
            LOGGER.debug("Application default properties: {}", defaultProp);

        return defaultProp;
    }

    private enum DefaultProperties {
        SERVICE_BASE_PATH("discovery.service.base.path", "/services"),
        ZOOKEEPER_CONNECTION_STRING("discovery.zookeeper.connection.string", ""),
        ZOOKEEPER_SESSION_TIMEOUT("discovery.zookeeper.session.timeout", "20000"),
        ZOOKEEPER_CONNECTION_TIMEOUT("discovery.zookeeper.connection.timeout", "15000"),
        JANITOR_REFRESH("discovery.janitor.refresh.time", "5000"),
        JETTY_MAIN_PORT("discovery.jetty.main.port", "9090");
        // TODO enable monitoring
        // JETTY_MONITORING_PORT("discovery.jetty.monitoring.port", "9091");

        private final String defaultKey;
        private final String defaulValue;

        private DefaultProperties(String defaultKey, String defaultValue) {
            this.defaultKey = defaultKey;
            this.defaulValue = defaultValue;
        }

        public String getDefaultKey() {
            return this.defaultKey;
        }

        public String getDefaulValue() {
            return this.defaulValue;
        }
    }

    private static int readInt(Properties prop, DefaultProperties defaultProps) throws Exception {
        int i = 0;

        String defaultPropValue = prop.getProperty(defaultProps.getDefaultKey());
        String str = System.getProperty(defaultProps.getDefaultKey(), defaultPropValue);

        try {
            i = Integer.parseInt(str);
        } catch (NumberFormatException e) {
            throw new Exception("Failed to read property [" + defaultProps.getDefaultKey() + "]", e);
        }

        return i;
    }

    private static String readString(Properties prop, DefaultProperties defaultProps, boolean acceptNull) throws Exception {
        String defaultPropValue = prop.getProperty(defaultProps.getDefaultKey());
        String str = System.getProperty(defaultProps.getDefaultKey(), defaultPropValue);;

        if (!acceptNull && (str == null || str.trim().isEmpty()))
            throw new Exception("Failed to read property [" + defaultProps.getDefaultKey() + "]");

        return str;
    }

}
