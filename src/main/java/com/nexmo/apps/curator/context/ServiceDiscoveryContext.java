package com.nexmo.apps.curator.context;

import javax.ws.rs.ext.ContextResolver;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.utils.CloseableUtils;
import org.apache.curator.x.discovery.ProviderStrategy;
import org.apache.curator.x.discovery.ServiceDiscovery;
import org.apache.curator.x.discovery.ServiceDiscoveryBuilder;
import org.apache.curator.x.discovery.details.JsonInstanceSerializer;
import org.apache.curator.x.discovery.server.contexts.GenericDiscoveryContext;
import org.apache.curator.x.discovery.server.rest.DiscoveryContext;
import org.apache.curator.x.discovery.server.rest.InstanceCleanup;
import org.apache.curator.x.discovery.strategies.RandomStrategy;
import org.apache.zookeeper.KeeperException.NodeExistsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServiceDiscoveryContext {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceDiscoveryContext.class);

    private final String serviceBasePath;

    private final CuratorFramework client;
    private final ServiceDiscovery<Void> serviceDiscovery;
    private final GenericDiscoveryContext<Void> discoveryContext;
    private final InstanceCleanup instanceHeartbeatJanitor;

    public ServiceDiscoveryContext(ServiceDiscoveryConfig config) {
        this.serviceBasePath = config.getServiceBasePath();

        this.client = CuratorFrameworkFactory.newClient(config.getConnectionString(),
                                                        config.getSessionTimeout(),
                                                        config.getConnectionTimeout(),
                                                        config.getRetryPolicy());

        this.serviceDiscovery = ServiceDiscoveryBuilder.builder(Void.class)
                                                       .client(this.client)
                                                       .basePath(config.getServiceBasePath())
                                                       .serializer(new JsonInstanceSerializer<Void>(Void.class))
                                                       .build();

        ProviderStrategy<Void> providerStrategy = new RandomStrategy<>();

        this.discoveryContext = new GenericDiscoveryContext<>(this.serviceDiscovery,
                                                              providerStrategy,
                                                              config.getInstanceRefreshTime(),
                                                              Void.class);

        this.instanceHeartbeatJanitor = new InstanceCleanup(this.serviceDiscovery,
                                                            config.getInstanceRefreshTime());
    }

    public void start() throws Exception {
        this.client.start();

        // create path in ZK if it doesn't exist
        try {
            this.client.create().forPath(this.serviceBasePath);
        } catch (NodeExistsException e) {
            LOGGER.warn("Failed to create path [{}] in zk because node already exists", this.serviceBasePath);
        } catch (Exception e) {
            LOGGER.error("Something serious happened while creating the node...", e);
            throw e;
        }

        this.instanceHeartbeatJanitor.start();
    }

    public void stop() {
        CloseableUtils.closeQuietly(this.instanceHeartbeatJanitor);
        CloseableUtils.closeQuietly(this.serviceDiscovery);
        CloseableUtils.closeQuietly(this.client);
    }

    public ContextResolver<DiscoveryContext<Void>> getContextResolver() {
        return this.discoveryContext;
    }
}
