package com.nexmo.apps.curator.context;

import org.apache.curator.RetryPolicy;

public class ServiceDiscoveryConfig {

    private final String connectionString;
    private final int sessionTimeout;
    private final int connectionTimeout;
    private final RetryPolicy retryPolicy;
    private final String serviceBasePath;
    private final int instanceRefreshTime;

    private ServiceDiscoveryConfig(String connectionString,
                                   int sessionTimeout,
                                   int connectionTimeout,
                                   RetryPolicy retryPolicy,
                                   String serviceBasePath,
                                   int instanceRefreshTime) {
        this.connectionString = connectionString;
        this.sessionTimeout = sessionTimeout;
        this.connectionTimeout = connectionTimeout;
        this.retryPolicy = retryPolicy;
        this.serviceBasePath = serviceBasePath;
        this.instanceRefreshTime = instanceRefreshTime;
    }

    public String getConnectionString() {
        return this.connectionString;
    }

    public int getSessionTimeout() {
        return this.sessionTimeout;
    }

    public int getConnectionTimeout() {
        return this.connectionTimeout;
    }

    public RetryPolicy getRetryPolicy() {
        return this.retryPolicy;
    }

    public String getServiceBasePath() {
        return this.serviceBasePath;
    }

    public int getInstanceRefreshTime() {
        return this.instanceRefreshTime;
    }

    public static class Builder {
        private String connectionString;
        private int sessionTimeout;
        private int connectionTimeout;
        private RetryPolicy retryPolicy;
        private String serviceBasePath;
        private int instanceRefreshTime;

        public Builder withConnectionString(String connectionString) {
            this.connectionString = connectionString;
            return this;
        }

        public Builder withSessionTimeout(int sessionTimeout) {
            this.sessionTimeout = sessionTimeout;
            return this;
        }

        public Builder withConnectionTimeout(int connectionTimeout) {
            this.connectionTimeout = connectionTimeout;
            return this;
        }

        public Builder withRetryPolicy(RetryPolicy retryPolicy) {
            this.retryPolicy = retryPolicy;
            return this;
        }

        public Builder withServiceBasePath(String serviceBasePath) {
            this.serviceBasePath = serviceBasePath;
            return this;
        }

        public Builder withInstanceRefreshTime(int instanceRefreshTime) {
            this.instanceRefreshTime = instanceRefreshTime;
            return this;
        }

        public ServiceDiscoveryConfig build() {
            return new ServiceDiscoveryConfig(this.connectionString,
                                              this.sessionTimeout,
                                              this.connectionTimeout,
                                              this.retryPolicy,
                                              this.serviceBasePath,
                                              this.instanceRefreshTime);
        }
    }
}
