package com.nexmo.apps.curator.resources;

import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.ContextResolver;

import org.apache.curator.x.discovery.server.rest.DiscoveryContext;
import org.apache.curator.x.discovery.server.rest.DiscoveryResource;

@Path("/no-payload")
public class NoPayloadDiscoveryResource extends DiscoveryResource<Void> {

    public NoPayloadDiscoveryResource(@Context ContextResolver<DiscoveryContext<Void>> resolver) {
        super(resolver.getContext(DiscoveryContext.class));
    }
}
