# marco-polo (Prototype)

marco-polo is a simple java RESTful service that allows you to register non JVM applications into a curator-backed service discovery platform. 

In a sense, it is similar to Netflix's [Prana](https://github.com/Netflix/Prana) project, as it allows you to *side-car* applications that don't speak JVM into a service discovery platform backed by [Apache Zookeeper](https://zookeeper.apache.org/) and the [Curator Framework](http://curator.apache.org/).

It also allows such platforms to discover other services registered in the backend.

The application is bundled in a uber-jar for ease of deployment. Just run:

```
./gradlew shadowJar
```

The uber-jar should be generated in /build/libs.

**NOTE**

* At the moment this service only allows you to register apps without app specific payloads. This will change in the future.
* This is a PROTOTYPE. So here be dragons...

## Starting up

The application already comes bundled with a default properties file. You can provide your own properties file through the command line. You can also override any property through Java's environment properties. For example:

```
# this will start jetty on port 5555 instead of the defaul 9090
$ java -jar marco-polo-uber.jar -Ddiscovery.jetty.main.port=5555
```

Make sure you have a zookeeper instance running somewhere as well!

## Usage examples

Assuming you started the application with the default properties, you can register a service (with no specific payload) by doing the following:

```
$ curl -i \
       -X PUT \
       -H "Content-Type: application/json" \
       --data '{ "name": "example", "id": "1", "address": "10.20.30.40", "port": 1234, "registrationTimeUTC": 1325129459728, "serviceType": "PERMANENT" }' \
       "http://127.0.0.1:9090/no-payload/v1/service/example/1"
```

You can retrieve that specific service by doing:

```
$ curl -i \
       -X GET \
       -H "Accept: application/json" \
       "http://127.0.0.1:9090/no-payload/v1/service/example/1"
```

Finally you can remove the service by doing:

```
$ curl -i \
       -X DELETE \
       "http://127.0.0.1:9090/no-payload/v1/service/example/1"
```

You can go [here](https://git-wip-us.apache.org/repos/asf?p=curator.git;a=blob_plain;f=curator-x-discovery-server/README.txt;hb=HEAD) for more info about Curator's Entity JSON format.

## Properties file format

```
discovery.service.base.path=/services

# zookeeper connection properties
discovery.zookeeper.connection.string=localhost:6666
discovery.zookeeper.session.timeout=20000
discovery.zookeeper.connection.timeout=15000

discovery.janitor.refresh.time=5000

# jetty properties
discovery.jetty.main.port=9090
```